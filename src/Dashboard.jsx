import React, {Component} from 'react';
import {connect} from 'react-redux';
import {changeMsgBtn, getMessageBegin, getMessageSuccess, getMessageFailure} from "./store/message/actions";
import {changePodBtn} from "./store/podcast/actions";

function mapStateToProps(state) {
    return {
        podbtncolor : state.podbtncolor,
        msgbtncolor : state.msgbtncolor,
    }
}

const mapDispatchToProps = {
    changeMsgBtn,
    changePodBtn,
    getMessageBegin,
    getMessageSuccess,
    getMessageFailure
};

class Dashboard extends Component {
    changeMsgColor = () => {
        this.props.changeMsgBtn();
    };

    changePodColor = () => {
        this.props.changePodBtn();
    };

    getMessage = () => {
        const id = Math.floor(Math.random() * 100);
        this.props.getMessageBegin();
        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
            .then(res => res.json())
            .then(json => {
                this.props.getMessageSuccess(json.title)
            })
            .catch(err => {
                this.props.getMessageFailure(err)
            })
    };

    render() {
        return (
            <div className={'dashboard'}>
                <button onClick={this.changePodColor}>change podbtn color</button>
                <button onClick={this.changeMsgColor}>change msgbtn color</button>
                <button onClick={this.getMessage}>get podcast</button>
                <button onClick={this.getMessage}>Get a message</button>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);