import './App.scss';
import Podcast from "./Podcast";
import Message from "./Message";
import Dashboard from "./Dashboard";
import React, {Component} from 'react';
import {connect} from 'react-redux';


function mapStateToProps(state) {
    return {...state}
}

class App extends Component {
  render() {
        return (
            <div className="App">
                <h1>Redux Test App</h1>
                <div className={`messagebox ${this.props.messagesReducer.showMessages ? 'show' : ''}`}>
                    <h1>this is a message box</h1>
                    <p>
                        {this.props.messagesReducer.loading ?
                            'LOADING ...'
                            :
                            this.props.messagesReducer.message
                        }
                    </p>
                </div>
                <div className={`podcastbox ${this.props.podcastsReducer.showPodcasts ? 'show' : ''}`}>
                    <h1>this is the podcast control section</h1>
                    <p>
                        {this.props.messagesReducer.loading ?
                            'LOADING ...'
                            :
                            this.props.messagesReducer.message
                        }
                    </p>
                </div>

                <Message/>
                <Podcast/>
                <Dashboard/>


            </div>
        );
    }
}

export default connect(mapStateToProps)(App);