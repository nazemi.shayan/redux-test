import React, {Component} from 'react';
import {connect} from 'react-redux';
import {togglePodcasts} from "./store/podcast/actions";

function mapStateToProps(state) {
    return {
        podbtncolor : state.podcastsReducer.podbtncolor,
    }
}

const mapDispatchToProps = {
    togglePodcasts
};

class Podcast extends Component {

    show = () => {
        this.props.togglePodcasts();
    };
    render() {
        return (
            <div>
                <button onClick={this.show} style={{backgroundColor : this.props.podbtncolor}}>Show Podcast Box</button>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Podcast);