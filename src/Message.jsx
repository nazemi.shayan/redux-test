import React, {Component} from 'react';
import {connect} from 'react-redux'
import {toggleMessages} from './store/message/actions'

function mapStateToProps(state) {
    console.log(state);
    return {
        msgbtncolor : state.messagesReducer.msgbtncolor,
    }
}

const mapDispatchToProps = {
    toggleMessages,
};

class Message extends Component {
    show = () => {
        this.props.toggleMessages();
    };
    render() {
        return (
            <div>
                <button onClick={this.show} style={{backgroundColor : this.props.msgbtncolor}}>Send Message</button>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Message);