export const TOGGLEPODCASTS = 'TOGGLEPODCASTS';
export const CHANGEPODBTN = 'CHANGEPODBTN';

export function togglePodcasts() {
    return {type : TOGGLEPODCASTS}
}

export function changePodBtn() {
    return {type : CHANGEPODBTN}
}