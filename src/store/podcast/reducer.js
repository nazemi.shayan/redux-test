import {CHANGEPODBTN, TOGGLEPODCASTS} from './actions';
import initialStates from './store';

export default function podcast(state=initialStates, action) {
    const colors = ['red', 'pink', 'yellow', 'blue', 'green'];
    switch(action.type) {
        case CHANGEPODBTN:
            return {
                ...state,
                podbtncolor : colors[Math.floor(Math.random() * 5)],
            };
        case TOGGLEPODCASTS:
            return {
                ...state,
                showPodcasts : !state.showPodcasts,
            };
        default :
            return state;
    }
}