import {combineReducers} from 'redux';
import messagesReducer from './message/reducer';
import podcastsReducer from './podcast/reducer';

export default combineReducers({
        messagesReducer,
        podcastsReducer
    })
