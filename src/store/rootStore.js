import messagesInitialStates from './message/store';
import podcastsInitialStates from './podcast/store';

export default {
    messagesInitialStates,
    podcastsInitialStates
}