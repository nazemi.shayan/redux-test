export const TOGGLEMESSAGES = 'TOGGLEMESSAGES';
export const CHANGEMSGBTN = 'CHANGEMSGBTN';

export const GET_MESSAGE_BEGIN = 'GET_MESSAGE_BEGIN';
export const GET_MESSAGE_SUCCESS = 'GET_MESSAGE_SUCCESS';
export const GET_MESSAGE_FAILURE = 'GET_MESSAGE_FAILURE';

export function toggleMessages() {
    return {type : TOGGLEMESSAGES}
}

export function changeMsgBtn() {
    return {type : CHANGEMSGBTN}
}

export function getMessageBegin() {
    return {type : GET_MESSAGE_BEGIN}
}

export function getMessageSuccess(data) {
    return (
        {
            type : GET_MESSAGE_SUCCESS,
            payload : { data },
        }
    )
}

export function getMessageFailure(error) {
    return (
        {
            type : GET_MESSAGE_FAILURE,
            payload : { error },
        }
    )
}