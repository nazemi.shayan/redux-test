import {
    CHANGEMSGBTN,
    TOGGLEMESSAGES,
    GET_MESSAGE_BEGIN,
    GET_MESSAGE_SUCCESS,
    GET_MESSAGE_FAILURE} from './actions';

import initialStates from './store';

export default function message(state=initialStates, action) {
    const colors = ['red', 'pink', 'yellow', 'blue', 'green'];
    switch(action.type) {
        case CHANGEMSGBTN:
            return {
                ...state,
                msgbtncolor : colors[Math.floor(Math.random() * 5)],
            };
        case TOGGLEMESSAGES:
            return {
                ...state,
                showMessages : !state.showMessages,
            };
        case GET_MESSAGE_BEGIN:
            return {
                ...state,
                loading : true,
                error : null,
            };
        case GET_MESSAGE_SUCCESS:
            return {
                ...state,
                message: action.payload.data,
                loading: false,
                error: null,
            };
        case GET_MESSAGE_FAILURE:
            return {
                ...state,
                loading : false,
                error : action.payload.error,
                message : '',
            };
        default :
            return state;
    }
}